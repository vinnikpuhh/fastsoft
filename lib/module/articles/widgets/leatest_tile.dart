import 'package:cached_network_image/cached_network_image.dart';
import 'package:fastsoft/index.dart';
import 'package:flutter/material.dart';

class LeatestTile extends StatelessWidget {
  const LeatestTile({
    super.key,
    required this.title,
    required this.publicationDate,
    required this.urlImage,
    required this.read,
    required this.onTap,
  });

  final Function() onTap;
  final bool read;
  final String title;
  final DateTime publicationDate;
  final String urlImage;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        height: 105.0,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(20.0),
        margin: const EdgeInsets.only(bottom: 20.0, left: 30.0, right: 30.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(9.0),
          border: Border.all(color: read ? Colors.transparent : Colors.red),
          boxShadow: [
            BoxShadow(
              color: ThemeColor.black.withOpacity(0.1),
              offset: const Offset(4, 4),
              blurRadius: 8,
            )
          ],
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(12.0),
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                height: 60.0,
                width: 90.0,
                imageUrl: urlImage,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    Center(
                  child: CircularProgressIndicator(
                    value: downloadProgress.progress,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 23.0),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: SoftTextStyle.px16,
                  ),
                  Text(
                    convertToAgo(publicationDate),
                    style: SoftTextStyle.px12secondary,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
