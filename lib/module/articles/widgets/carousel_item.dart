import 'package:cached_network_image/cached_network_image.dart';
import 'package:fastsoft/index.dart';
import 'package:flutter/material.dart';

class CarouselItem extends StatelessWidget {
  const CarouselItem({
    super.key,
    required this.read,
    required this.title,
    required this.imageUrl,
    required this.onTap,
  });
  final Function() onTap;
  final bool read;
  final String title;
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            border: Border.all(
                color: read ? Colors.transparent : Colors.red, width: 2)),
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: CachedNetworkImage(
                imageUrl: imageUrl,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    Center(
                  child: CircularProgressIndicator(
                    value: downloadProgress.progress,
                  ),
                ),
                fit: BoxFit.fill,
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.33,
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                margin: const EdgeInsets.only(left: 20.0, bottom: 40.0),
                width: MediaQuery.of(context).size.width * 0.67,
                child: Text(
                  title,
                  style: SoftTextStyle.px28white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
