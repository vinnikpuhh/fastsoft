import 'package:cached_network_image/cached_network_image.dart';
import 'package:fastsoft/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SingleArticle extends StatelessWidget {
  const SingleArticle({
    super.key,
  });

  static const String routeName = '/single-aticle';

  @override
  Widget build(BuildContext context) {
    Size mq = MediaQuery.of(context).size;
    return BlocBuilder<ArticleCubit, ArticleState>(
      builder: (context, state) {
        return Scaffold(
          body: state.status.loading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : CustomScrollView(
                  slivers: <Widget>[
                    SliverAppBar(
                      automaticallyImplyLeading: false,
                      leading: IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: SvgPicture.asset(SvgIcon.leftShevron),
                      ),
                      pinned: true,
                      expandedHeight: mq.height * 0.5,
                      collapsedHeight: 150,
                      flexibleSpace: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: Stack(
                          children: [
                            CachedNetworkImage(
                              height: mq.height * 0.6,
                              width: mq.width,
                              fit: BoxFit.cover,
                              imageUrl: state.currentArticle?.imageUrl ??
                                  'https://i.pinimg.com/originals/ab/0c/cd/ab0ccd195f693a98973735817e9aa7de.jpg',
                              progressIndicatorBuilder:
                                  (context, url, downloadProgress) => Center(
                                child: CircularProgressIndicator(
                                  value: downloadProgress.progress,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Container(
                                margin: const EdgeInsets.only(
                                  left: 40.0,
                                  right: 40.0,
                                  bottom: 48.0,
                                ),
                                width: mq.width * 0.8,
                                child: Text(
                                  state.currentArticle?.title ?? '',
                                  style: SoftTextStyle.px28white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Padding(
                        padding: const EdgeInsets.all(21.0),
                        child: Text(
                          '${state.currentArticle?.description}\n\n${state.currentArticle?.description}\n\n\n${state.currentArticle?.description}\n\n\n${state.currentArticle?.description}\n',
                          style: SoftTextStyle.px16,
                        ),
                      ),
                    )
                  ],
                ),
        );
      },
    );
  }
}
