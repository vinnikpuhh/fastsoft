import 'dart:math';
import 'package:fastsoft/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';

class Articles extends StatefulWidget {
  const Articles({super.key});

  static const String routeName = '/articles';

  @override
  State<Articles> createState() => _ArticlesState();
}

class _ArticlesState extends State<Articles> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ArticleCubit>(context).getArticleFeatured();
    BlocProvider.of<ArticleCubit>(context).getArticleLeatest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //TODO По хорошему скаффолд надо в отдельный компонент выносить, но сейчас смысла нет
      appBar: AppBar(
        surfaceTintColor: Colors.white,
        title: const Text(
          'Notifications',
          style: SoftTextStyle.px18,
        ),
        centerTitle: true,
        actions: [
          //TODO кастом кнопки происходит стандартный через style, на данный момент конкретных указаний на дизайн самой кнопки а не тайтла нет
          TextButton(
            onPressed: () => BlocProvider.of<ArticleCubit>(context).readAll(),
            child: const Text(
              'Mark all read',
              style: SoftTextStyle.px18,
            ),
          ),
        ],
      ),
      body: BlocBuilder<ArticleCubit, ArticleState>(
        builder: (context, state) {
          if (state.status.loading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return CustomScrollView(
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: articleTitle('Featured'),
              ),
              featureCarousel(state),
              SliverToBoxAdapter(
                child: articleTitle('Latest news'),
              ),
              leatestArticlesListing(state),
            ],
          );
        },
      ),
    );
  }

  Widget articleTitle(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
      child: Text(
        title,
        style: SoftTextStyle.px20,
      ),
    );
  }

  Widget featureCarousel(ArticleState state) {
    return SliverPersistentHeader(
      floating: true,
      pinned:
          false, //TODO если нужно чтобы этот делегат был закреплен, изменить на true
      delegate: _SliverAppBarDelegate(
        minHeight: 105.0,
        maxHeight: MediaQuery.of(context).size.height * 0.33,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: FlutterCarousel(
            options: CarouselOptions(
              viewportFraction: 1,
              autoPlay: true,
              showIndicator: false,
              enableInfiniteScroll: true,
              clipBehavior: Clip.hardEdge,
              slideIndicator: const CircularSlideIndicator(),
            ),
            items: (state.articleFeatured ?? []).map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return CarouselItem(
                    onTap: () =>
                        goToArticle(id: i.id, type: ArticleType.featured),
                    read: i.read ?? false,
                    title: i.title,
                    imageUrl: i.imageUrl,
                  );
                },
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  goToArticle({required String id, required ArticleType type}) {
    ArticleCubit cubit = BlocProvider.of<ArticleCubit>(context);
    cubit.getCurrentArticle(
      id: id,
      type: type,
    );
    cubit.readCurrentArticle(
      id: id,
      type: type,
    );
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => const SingleArticle(),
      ),
    );
  }

  Widget leatestArticlesListing(ArticleState state) {
    return SliverList.builder(
      itemCount:
          BlocProvider.of<ArticleCubit>(context).state.articleLatest?.length ??
              0,
      itemBuilder: (BuildContext context, int index) {
        Article art =
            BlocProvider.of<ArticleCubit>(context).state.articleLatest![index];
        return LeatestTile(
          read: art.read ?? false,
          title: art.title,
          publicationDate: art.publicationDate,
          urlImage: art.imageUrl,
          onTap: () => goToArticle(id: art.id, type: ArticleType.leatest),
        );
      },
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });
  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
