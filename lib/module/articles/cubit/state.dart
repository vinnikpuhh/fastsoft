import 'package:fastsoft/index.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'state.freezed.dart';

enum ArticleStatus {
  init,
  loading,
  success,
  failure,
}

extension ArticleStatusX on ArticleStatus {
  bool get init => this == ArticleStatus.init;
  bool get loading => this == ArticleStatus.loading;
  bool get success => this == ArticleStatus.success;
  bool get failure => this == ArticleStatus.failure;
}

@freezed
class ArticleState with _$ArticleState {
  factory ArticleState({
    required ArticleStatus status,
    List<Article>? articleFeatured,
    List<Article>? articleLatest,
    Article? currentArticle,
  }) = _ArticleState;
}
