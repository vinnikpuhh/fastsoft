// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ArticleState {
  ArticleStatus get status => throw _privateConstructorUsedError;
  List<Article>? get articleFeatured => throw _privateConstructorUsedError;
  List<Article>? get articleLatest => throw _privateConstructorUsedError;
  Article? get currentArticle => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ArticleStateCopyWith<ArticleState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ArticleStateCopyWith<$Res> {
  factory $ArticleStateCopyWith(
          ArticleState value, $Res Function(ArticleState) then) =
      _$ArticleStateCopyWithImpl<$Res, ArticleState>;
  @useResult
  $Res call(
      {ArticleStatus status,
      List<Article>? articleFeatured,
      List<Article>? articleLatest,
      Article? currentArticle});

  $ArticleCopyWith<$Res>? get currentArticle;
}

/// @nodoc
class _$ArticleStateCopyWithImpl<$Res, $Val extends ArticleState>
    implements $ArticleStateCopyWith<$Res> {
  _$ArticleStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? articleFeatured = freezed,
    Object? articleLatest = freezed,
    Object? currentArticle = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ArticleStatus,
      articleFeatured: freezed == articleFeatured
          ? _value.articleFeatured
          : articleFeatured // ignore: cast_nullable_to_non_nullable
              as List<Article>?,
      articleLatest: freezed == articleLatest
          ? _value.articleLatest
          : articleLatest // ignore: cast_nullable_to_non_nullable
              as List<Article>?,
      currentArticle: freezed == currentArticle
          ? _value.currentArticle
          : currentArticle // ignore: cast_nullable_to_non_nullable
              as Article?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ArticleCopyWith<$Res>? get currentArticle {
    if (_value.currentArticle == null) {
      return null;
    }

    return $ArticleCopyWith<$Res>(_value.currentArticle!, (value) {
      return _then(_value.copyWith(currentArticle: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ArticleStateImplCopyWith<$Res>
    implements $ArticleStateCopyWith<$Res> {
  factory _$$ArticleStateImplCopyWith(
          _$ArticleStateImpl value, $Res Function(_$ArticleStateImpl) then) =
      __$$ArticleStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {ArticleStatus status,
      List<Article>? articleFeatured,
      List<Article>? articleLatest,
      Article? currentArticle});

  @override
  $ArticleCopyWith<$Res>? get currentArticle;
}

/// @nodoc
class __$$ArticleStateImplCopyWithImpl<$Res>
    extends _$ArticleStateCopyWithImpl<$Res, _$ArticleStateImpl>
    implements _$$ArticleStateImplCopyWith<$Res> {
  __$$ArticleStateImplCopyWithImpl(
      _$ArticleStateImpl _value, $Res Function(_$ArticleStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? articleFeatured = freezed,
    Object? articleLatest = freezed,
    Object? currentArticle = freezed,
  }) {
    return _then(_$ArticleStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ArticleStatus,
      articleFeatured: freezed == articleFeatured
          ? _value._articleFeatured
          : articleFeatured // ignore: cast_nullable_to_non_nullable
              as List<Article>?,
      articleLatest: freezed == articleLatest
          ? _value._articleLatest
          : articleLatest // ignore: cast_nullable_to_non_nullable
              as List<Article>?,
      currentArticle: freezed == currentArticle
          ? _value.currentArticle
          : currentArticle // ignore: cast_nullable_to_non_nullable
              as Article?,
    ));
  }
}

/// @nodoc

class _$ArticleStateImpl implements _ArticleState {
  _$ArticleStateImpl(
      {required this.status,
      final List<Article>? articleFeatured,
      final List<Article>? articleLatest,
      this.currentArticle})
      : _articleFeatured = articleFeatured,
        _articleLatest = articleLatest;

  @override
  final ArticleStatus status;
  final List<Article>? _articleFeatured;
  @override
  List<Article>? get articleFeatured {
    final value = _articleFeatured;
    if (value == null) return null;
    if (_articleFeatured is EqualUnmodifiableListView) return _articleFeatured;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Article>? _articleLatest;
  @override
  List<Article>? get articleLatest {
    final value = _articleLatest;
    if (value == null) return null;
    if (_articleLatest is EqualUnmodifiableListView) return _articleLatest;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final Article? currentArticle;

  @override
  String toString() {
    return 'ArticleState(status: $status, articleFeatured: $articleFeatured, articleLatest: $articleLatest, currentArticle: $currentArticle)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ArticleStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality()
                .equals(other._articleFeatured, _articleFeatured) &&
            const DeepCollectionEquality()
                .equals(other._articleLatest, _articleLatest) &&
            (identical(other.currentArticle, currentArticle) ||
                other.currentArticle == currentArticle));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      const DeepCollectionEquality().hash(_articleFeatured),
      const DeepCollectionEquality().hash(_articleLatest),
      currentArticle);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ArticleStateImplCopyWith<_$ArticleStateImpl> get copyWith =>
      __$$ArticleStateImplCopyWithImpl<_$ArticleStateImpl>(this, _$identity);
}

abstract class _ArticleState implements ArticleState {
  factory _ArticleState(
      {required final ArticleStatus status,
      final List<Article>? articleFeatured,
      final List<Article>? articleLatest,
      final Article? currentArticle}) = _$ArticleStateImpl;

  @override
  ArticleStatus get status;
  @override
  List<Article>? get articleFeatured;
  @override
  List<Article>? get articleLatest;
  @override
  Article? get currentArticle;
  @override
  @JsonKey(ignore: true)
  _$$ArticleStateImplCopyWith<_$ArticleStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
