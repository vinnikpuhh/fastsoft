import 'package:fastsoft/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ArticleType { featured, leatest }

class ArticleCubit extends Cubit<ArticleState> {
/*   ArticleApi articleApi = ArticleApi(); */

  ArticleCubit()
      : super(
          ArticleState(
            status: ArticleStatus.init,
          ),
        );

  // #region "Прочитать" конкретную статью

  readCurrentArticle({
    required String id,
    required ArticleType type, //TODO эта переменная нужна из за отсутствия бека
  }) async {
    List<Article>? featured = [...(state.articleFeatured ?? [])];
    List<Article>? latest = [...(state.articleLatest ?? [])];

    await Future.delayed(const Duration(seconds: 2), () async {
      switch (type) {
        case ArticleType.featured:
          if (featured.isNotEmpty) {
            final index = featured.indexWhere((e) => e.id == id);
            if (index > -1) {
              featured.insert(
                index,
                Article(
                  id: featured[index].id,
                  title: featured[index].title,
                  publicationDate: featured[index].publicationDate,
                  imageUrl: featured[index].imageUrl,
                  division: featured[index].division,
                  read: true,
                  description: featured[index].description,
                ),
              );
              featured.removeAt(index + 1);
            }
          } else {
            emit(
              state.copyWith(
                status: ArticleStatus.failure,
              ),
            );
            return;
          }
        case ArticleType.leatest:
          if (latest.isNotEmpty) {
            final index = latest.indexWhere((e) => e.id == id);
            if (index > -1) {
              latest.insert(
                index,
                Article(
                  id: latest[index].id,
                  title: latest[index].title,
                  publicationDate: latest[index].publicationDate,
                  imageUrl: latest[index].imageUrl,
                  division: latest[index].division,
                  read: true,
                  description: latest[index].description,
                ),
              );
              latest.removeAt(index + 1);
            }
          } else {
            emit(
              state.copyWith(
                status: ArticleStatus.failure,
              ),
            );
            return;
          }
      }
    });

    emit(
      state.copyWith(
        status: ArticleStatus.success,
        articleFeatured:
            type == ArticleType.featured ? featured : state.articleFeatured,
        articleLatest:
            type == ArticleType.leatest ? latest : state.articleFeatured,
      ),
    );
  }

  // #endregion

  // #region "Прочитать" все статьи

  readAll() {
    //TODO Обычно для прочтения всего есть метод на беке будем считать он тут есть
    List<Article> featured = [];
    List<Article> leatest = [];
    for (var val in state.articleFeatured!) {
      featured.add(
        Article(
          id: val.id,
          title: val.title,
          publicationDate: val.publicationDate,
          imageUrl: val.imageUrl,
          division: val.division,
          read: true,
          description: val.description,
        ),
      );
    }
    for (var val in state.articleLatest!) {
      leatest.add(
        Article(
          id: val.id,
          title: val.title,
          publicationDate: val.publicationDate,
          imageUrl: val.imageUrl,
          division: val.division,
          read: true,
          description: val.description,
        ),
      );
    }

    emit(state.copyWith(
      articleFeatured: featured,
      articleLatest: leatest,
    ));
  }

  // #endregion

  // #region получение конкретной статьи по айдишнику

  getCurrentArticle({
    required String id,
    required,
    required ArticleType type, //TODO эта переменная нужна из за отсутствия бека
  }) async {
    emit(
      state.copyWith(
        status: ArticleStatus.loading,
      ),
    );

// #region имитация запроса на бек

    Article? article;

    await Future.delayed(const Duration(seconds: 2), () async {
      switch (type) {
        case ArticleType.featured:
          if (state.articleFeatured != null) {
            article = state.articleFeatured!.firstWhere((e) => e.id == id);
          } else {
            emit(
              state.copyWith(
                status: ArticleStatus.failure,
              ),
            );
            return;
          }
        case ArticleType.leatest:
          if (state.articleLatest != null) {
            article = state.articleLatest!.firstWhere((e) => e.id == id);
          } else {
            emit(
              state.copyWith(
                status: ArticleStatus.failure,
              ),
            );
            return;
          }
      }
    });

// #endregion

    emit(
      state.copyWith(
        status: ArticleStatus.success,
        currentArticle: article,
      ),
    );
  }

  // #endregion

  // #region Получаем массив статей featured

  getArticleFeatured() async {
    emit(
      state.copyWith(
        status: ArticleStatus.loading,
      ),
    );

// #region имитация запроса на бек

    List<Article>? result;

    await Future.delayed(const Duration(seconds: 2), () async {
      result = await MockNewsRepository().getFeaturedArticles();
    });

// #endregion

    /*final result = await articleApi.getArticle();

    if (!result.success) {
      emit(
        state.copyWith(
          status: ArticleStatus.failure,
        ),
      );
      return;
    } */

    emit(
      state.copyWith(
        status: ArticleStatus.success,
        articleFeatured: result,
      ),
    );
  }

  // #endregion

  // #region Получаем массив статей leatest

  getArticleLeatest() async {
    emit(
      state.copyWith(
        status: ArticleStatus.loading,
      ),
    );

    // #region имитация запрос на бек

    List<Article>? result;

    await Future.delayed(const Duration(seconds: 2), () async {
      result = await MockNewsRepository().getLatestArticles();
    });

    // #endregion

    /*final result = await articleApi.getArticle();

    if (!result.success) {
      emit(
        state.copyWith(
          status: ArticleStatus.failure,
        ),
      );
      return;
    } */

    emit(
      state.copyWith(
        status: ArticleStatus.success,
        articleLatest: result,
      ),
    );
  }

  // #endregion
}
