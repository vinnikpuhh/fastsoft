import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'module/index.dart';
import 'shared/index.dart';

void main() {
  runApp(const ForestVPNTestApp());
}

class ForestVPNTestApp extends StatelessWidget {
  const ForestVPNTestApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
            create: (context) => ArticleCubit(),
      child: const MaterialApp(
        title: 'ForestVPN test',
        onGenerateRoute: RouterCfg.generateRoute,
        initialRoute: Articles.routeName,
      ),
    );
  }
}
