// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ArticleImpl _$$ArticleImplFromJson(Map<String, dynamic> json) =>
    _$ArticleImpl(
      id: json['id'] as String,
      title: json['title'] as String,
      publicationDate: DateTime.parse(json['publicationDate'] as String),
      imageUrl: json['imageUrl'] as String,
      division: json['division'] as bool? ?? false,
      read: json['read'] as bool? ?? false,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$$ArticleImplToJson(_$ArticleImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'publicationDate': instance.publicationDate.toIso8601String(),
      'imageUrl': instance.imageUrl,
      'division': instance.division,
      'read': instance.read,
      'description': instance.description,
    };
