import 'package:flutter/material.dart';

class SvgIcon {
  static const String leftShevron = 'assets/svg/left_shevron.svg';
}

class PngIcon {}

class ThemeColor {
  static const Color black = Color(0xFF000000);
  static const Color secondary = Color(0xFF9A9A9A);
// #region главный цвет приложения

  static const int mainColor = 0xFF000000;

  static const MaterialColor mainMaterialColor = MaterialColor(
    mainColor,
    <int, Color>{
      50: Color.fromRGBO(0, 0, 0, .1),
      100: Color.fromRGBO(0, 0, 0, .2),
      200: Color.fromRGBO(0, 0, 0, .3),
      300: Color.fromRGBO(0, 0, 0, .4),
      400: Color.fromRGBO(0, 0, 0, .5),
      500: Color.fromRGBO(0, 0, 0, .6),
      600: Color.fromRGBO(0, 0, 0, .7),
      700: Color.fromRGBO(0, 0, 0, .8),
      800: Color.fromRGBO(0, 0, 0, .9),
      900: Color.fromRGBO(0, 0, 0, 1),
    },
  );

// #endregion
}

class SoftTextStyle {
  static const TextStyle px16 = TextStyle(
    color: ThemeColor.black,
    fontWeight: FontWeight.w400,
    fontSize: 16.0,
    height: 1.2,
  );
  static const TextStyle px18 = TextStyle(
    color: ThemeColor.black,
    fontWeight: FontWeight.w400,
    fontSize: 18.0,
    height: 1.2,
  );
  static const TextStyle px20 = TextStyle(
    color: ThemeColor.black,
    fontWeight: FontWeight.w400,
    fontSize: 20.0,
    height: 1.2,
  );

  static const TextStyle px28white = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w400,
    fontSize: 28.0,
    height: 1.2,
  );

  static const TextStyle px12secondary = TextStyle(
    color: ThemeColor.secondary,
    fontWeight: FontWeight.w400,
    fontSize: 12.0,
    height: 1.2,
  );
}
