import 'package:fastsoft/index.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class RouterCfg {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // TODO еще должна быть проверка на наличие токена, если проверка не пройдена, перенаправляем на авторизацию

      case Articles.routeName:
        return PageTransition(
          child: const Articles(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case SingleArticle.routeName:
        return PageTransition(
          child: const SingleArticle(),
          type: PageTransitionType.fade,
          settings: settings,
        );

      default:
        //TODO тут как правило лежит как заглушка авторизация
    }
  }
}
